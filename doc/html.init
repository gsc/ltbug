# Texi2any configuration for ltbug1 documentation. -*- perl -*-
# Copyright (C) 2009-2024 Sergey Poznyakoff
use strict;

# Show TOC in place of the @contents directive.
set_from_init_file('INLINE_CONTENTS', 1);
# Do not show Texinfo menus.
set_from_init_file('SHOW_MENU', 0);
# Inhibit output of CSS lines in page headers.
set_from_init_file('CSS_LINES', '');

set_from_init_file('BODYTEXT', "");

set_from_init_file('EXTRA_HEAD', "\
  <link rev=\"made\" href=\"mailto:gray\@gnu.org.ua\">\
  <link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\">");

sub gray_end_file($)
{
    my $self = shift;
    my $program_text = '';
    if ($self->get_conf('PROGRAM_NAME_IN_FOOTER')) {
	my $program_string = &{$self->{'format_program_string'}}($self);
	$program_text = "<p>
  $program_string
</p>";
  }
  my $pre_body_close = $self->get_conf('PRE_BODY_CLOSE');
  $pre_body_close = '' if (!defined($pre_body_close));
  return "
<div class=\"copyright\">
$program_text

$pre_body_close
</div>
</body>
</html>
";
}

texinfo_register_formatting_function('end_file', \&gray_end_file);

set_from_init_file('PRE_BODY_CLOSE',
'<p>Copyright (C) 2019 &mdash; 2024 <a href="mailto:gray@gnu.org">Sergey Poznyakoff</a></p>
<p>		   
Content is available under <a href="http://www.gnu.org/copyleft/fdl.html">GNU Free Documentation License 1.3</a>.
</p>');

# Print generating program name at the bottom of a page.
set_from_init_file('PROGRAM_NAME_IN_FOOTER',1);

# Disable horizontal bars
set_from_init_file('DEFAULT_RULE', '');
set_from_init_file('BIG_RULE', '');

