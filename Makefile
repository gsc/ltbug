all: ltbug-1.tar.gz ltbug-2.tar.gz

ltbug-1.tar.gz: ltb1-autoconf
	rm -rf _build
	mkdir _build
	(cd _build && \
	 ../ltb1/configure && \
	 make dist)
	mv _build/ltbug-1.tar.gz .
	rm -rf _build

ltb1-autoconf:
	cd ltb1 && autoreconf -i -s

ltbug-2.tar.gz: ltb2-autoconf
	make -C ltb2 dist
	mv ltb2/ltbug-2.tar.gz .

ltb2-autoconf:
	cd ltb2/ltb2dep && autoreconf -i -s
	cd ltb2/ltb2 && autoreconf -i -s

ifneq (,$(wildcard .makevars))
 include .makevars
else
 $(error Required file .makevars does not exist)
endif

publish: all
	$(MAKE) -C doc publish
	$(MAKE) -C patches publish
	scp ltbug-1.tar.gz ltbug-2.tar.gz $(DISTLOC)
