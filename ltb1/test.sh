#! /bin/sh

set -e

cwdir=$(pwd)
topdir=$(dirname $0)
abs_topdir=$(cd $topdir; pwd)

testdir=$cwdir
prefix=${TMP:-/tmp}
keep=no
keep_testdir=no

help() {
    cat <<EOF
usage: $0 [OPTIONS]
Runs LTB1 test.
See http://gray.gnu.org.ua/public/ltbug#ltb1 for details.
OPTIONS are:

  -p, --prefix=DIR     set installation prefix root
                       (default: $prefix)
  -C, --directory=DIR  set test directory
                       (default: $testdir)
  --keep               keep test and installation directories for inspection
                       (default if the test fails)
  --help               display this help list

EOF
}

unset arg
while [ $# -gt 0 ]
do
    if [ -n "$arg" ]; then
	eval $arg="$1"
	unset arg
    else
	case $1 in
	--prefix)
	    arg=prefix
	    ;;
	--prefix=*)
	    prefix=${1##--prefix=}
	    ;;
	-h|--help)
	    help
	    exit 0
	    ;;
	-C|--directory)
	    arg=testdir
	    ;;
	-C*)
	    testdir=${1##-C}
	    ;;
	--directory=*)
	    testdir=${1##--directory=}
	    ;;
	--keep)
	    keep=yes
	    ;;
	*)
	    help >&2
	    exit 2
        esac
    fi
    shift
done

prefix=$(cd $prefix; pwd)/ltb-test1
testdir=$(cd $testdir; pwd)/ltb-test1

if [ $prefix = $testdir ]; then
    echo >&2 "prefix and test directory must differ"
    exit 2
fi    

STDOUT=$testdir/ltb1.out
STDERR=$testdir/ltb1.err

if [ -d $testdir ]; then
    keep_testdir=yes
else    
    mkdir $testdir
fi
if [ ! -w $testdir ]; then
    echo >&2 "$0: test directory ($testdir) must be writable"
    exit 2
fi
if [ ! -d $prefix ]; then
    mkdir $prefix
fi
if [ ! -w $prefix ]; then
    echo >&2 "$0: prefix directory ($prefix) must be writable"
    exit 2
fi
    
abend() {
    echo >&2 "Failed!"
    tail -n 5 $STDERR >&2
    echo >&2 "Examine $STDOUT and $STDERR for details"
    exit 2
}

cd $testdir

rm -rf build_inst build_local
mkdir build_inst build_local

echo "Building and installing project"
(
    cd build_inst
    $abs_topdir/configure --prefix=$prefix LTBUG_ID='installed'
    make
    make install
) 2>>$STDERR >$STDOUT || abend

echo "Building local copy of the project"
(
    cd build_local
    $abs_topdir/configure --prefix=$prefix
    make
) 2>>$STDERR >$STDOUT || abend

cat > expout <<EOF
ltbug: local
EOF

echo "Running test program"

build_local/src/ltb1 > stdout

if cmp -s stdout expout; then
    status=0
else
    keep=yes
    echo >&2 "Test failed:"
    diff -pu expout stdout || true
    status=1
fi    

cd $cwdir

if [ "$keep" = "yes" ]; then
    echo "Keeping the directories in place.  When no longer needed, run:"
    dry_run=echo
else
    echo "Uninstalling"
    dry_run=
fi

$dry_run rm -rf $prefix

if [ "$keep_testdir" = "yes" ]; then
    $dry_run rm -rf $testdir/build_inst $testdir/build_local
    $dry_run rm -f $STDERR $STDOUT
else
    $dry_run rm -rf $testdir
fi    

exit $status

